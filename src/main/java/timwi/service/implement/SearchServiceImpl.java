package timwi.service.implement;

import com.wrapper.spotify.model_objects.specification.AlbumSimplified;
import com.wrapper.spotify.model_objects.specification.Paging;
import org.springframework.stereotype.Service;
import timwi.config.SearchHelper;
import timwi.domain.Album;
import timwi.domain.Model;
import timwi.service.LibraryService;
import timwi.service.SearchService;

import java.util.ArrayList;
import java.util.List;

/**
 * 06/05/2021 - 08:41
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
@Service
public class SearchServiceImpl implements SearchService {

    private final LibraryService libraryService ;

    public SearchServiceImpl(LibraryService libraryService) {
        this.libraryService = libraryService;
    }

    @Override
    public List<Model> searchAlbums(String s, Long idLibrary) {
        List<Model> results = new ArrayList<Model>();
        List<Album> albumsLibrary = libraryService.getAllAlbumsFromLibrary(idLibrary);


        Paging<AlbumSimplified> result = SearchHelper.getAlbumSearchItems(s);
        AlbumSimplified[] albums = result.getItems();

        for(AlbumSimplified album : albums) {

            Model albumBean = new Model();
            albumBean.setName(album.getName());
            albumBean.setAlbumType(album.getAlbumType());
            albumBean.setImages(album.getImages());
            albumBean.setArtists(album.getArtists());
            albumBean.setId(album.getId());

            for(Album albumLibrary : albumsLibrary) {
                if(album.getId().equals(albumLibrary.getId())) {
                    albumBean.setInAlbumLibrary(true);
                }
            }

            results.add(albumBean);
        }

        return results;    }
}
