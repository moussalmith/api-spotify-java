package timwi.service.implement;

import org.springframework.stereotype.Service;
import timwi.dao.AlbumRepository;
import timwi.dao.TagRepository;
import timwi.domain.Album;
import timwi.domain.Tag;
import timwi.service.AlbumService;

import java.util.Optional;

/**
 * 06/05/2021 - 06:56
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
@Service
public class AlbumServiceImpl implements AlbumService {

    private final AlbumRepository albumRepository;
    private final TagRepository tagRepository;

    public AlbumServiceImpl(AlbumRepository albumRepository, TagRepository tagRepository) {
        this.albumRepository = albumRepository;
        this.tagRepository = tagRepository;
    }

    @Override
    public void addTagToAlbum(Long idAlbum, String tagName) {
            // uniformer le nom du tag en miniscule
            String name = tagName.trim().toLowerCase();
            Optional<Tag> tag = tagRepository.findByName(name);
            Optional<Album> album = albumRepository.findById(idAlbum);
            if (album.isPresent() && tag.isPresent()){
                tag.get().setName(name);
                album.get().getTags().add(tag.get());
            }
            albumRepository.save(album.get());
    }
}
