package timwi.service.implement;

import org.springframework.stereotype.Service;
import timwi.config.AlbumHelper;
import timwi.dao.AlbumRepository;
import timwi.dao.LibraryRepository;
import timwi.domain.Album;
import timwi.domain.Library;
import timwi.domain.Model;
import timwi.domain.Tag;
import timwi.service.LibraryService;

import java.util.*;

/**
 * 06/05/2021 - 07:30
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
@Service
public class LibraryServiceImpl implements LibraryService {

    private final LibraryRepository libraryRepository;
    private final AlbumRepository albumRepository;

    public LibraryServiceImpl(LibraryRepository libraryRepository, AlbumRepository albumRepository) {
        this.libraryRepository = libraryRepository;
        this.albumRepository = albumRepository;
    }

    @Override
    public void addAlbumToLibrary(String idAlbumSpotify) {
        var album = (Set<Album>) new Album(idAlbumSpotify);
        Library library = new Library(album);
        libraryRepository.save(library);
    }

    @Override
    public void deleteAlbumFromLibrary(String idAlbumSpotify) {

    }

    @Override
    public List<Album> getAllAlbumsFromLibrary(Long idLibrary) {
        Optional<Library> library = libraryRepository.findById(idLibrary);
        List<Album> modelList = null;
        if (library.isPresent()){
             modelList = (List<Album>) library.get().getAlbums();
        }
        return modelList;
    }

    @Override
    public List<Album> getAllAlbumsFromLibrary() {
        return null ;
    }

    @Override
    public void addOrRemoveFavoris(Long idAlbumSpotify, boolean favoris) {
        Optional<Album> album = albumRepository.findById(idAlbumSpotify);
        if(album.isPresent()) {
            album.get().setFavoris(!favoris);
            albumRepository.save(album.get());
        }
    }
}
