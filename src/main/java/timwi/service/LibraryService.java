package timwi.service;

import timwi.domain.Album;

import java.util.List;
import java.util.Set;

/**
 * 06/05/2021 - 06:32
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public interface LibraryService {

    void addAlbumToLibrary(String idAlbumSpotify);

    void deleteAlbumFromLibrary(String idAlbumSpotify);

    List<Album> getAllAlbumsFromLibrary(Long idLibrary);
    List<Album> getAllAlbumsFromLibrary();

    void addOrRemoveFavoris(Long idAlbumSpotify, boolean favoris);

}
