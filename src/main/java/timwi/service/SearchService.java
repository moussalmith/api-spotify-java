package timwi.service;

import com.mongodb.lang.Nullable;
import timwi.domain.Album;
import timwi.domain.Model;

import java.util.List;

/**
 * 06/05/2021 - 08:42
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public interface SearchService {
    List<Model> searchAlbums(String s, @Nullable Long id);
}
