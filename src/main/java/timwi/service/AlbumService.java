package timwi.service;

/**
 * 06/05/2021 - 06:30
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public interface AlbumService {

    void addTagToAlbum(Long id, String tag);
}
