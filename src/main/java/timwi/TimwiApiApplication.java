package timwi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimwiApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TimwiApiApplication.class, args);
    }

}
