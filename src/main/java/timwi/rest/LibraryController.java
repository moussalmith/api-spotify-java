package timwi.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import timwi.domain.Album;
import timwi.domain.Model;
import timwi.service.AlbumService;
import timwi.service.LibraryService;

import java.util.List;

/**
 * 06/05/2021 - 09:11
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
@RestController
@RequestMapping(value = "api")
public class LibraryController {
    private final AlbumService albumService;

    private final LibraryService libraryService;

    public LibraryController(AlbumService albumService, LibraryService libraryService) {
        this.albumService = albumService;
        this.libraryService = libraryService;
    }

    @GetMapping(value="/library/add/{idAlbum}")
    public boolean addAlbumToLibrary(@PathVariable("idAlbum") String idAlbumSpotify){
        libraryService.addAlbumToLibrary(idAlbumSpotify);
        return true;
    }

    @GetMapping(value="/library/delete/{idAlbum}")
    public boolean deleteAlbumFromLibrary(@PathVariable("idAlbum") String idAlbumSpotify){
        libraryService.deleteAlbumFromLibrary(idAlbumSpotify);
        return true;
    }

    @GetMapping(value="/library")
    public List<Album> getAllAlbumsFromLibrary(){
        List<Album> albums = libraryService.getAllAlbumsFromLibrary();
        return albums;
    }

    @GetMapping(value="/library/addOrRemoveFavoris/{idAlbum}/{favoris}")
    public boolean addOrRemoveFavoris(@PathVariable("idAlbum") Long idAlbumSpotify, @PathVariable("favoris") boolean favoris){
        libraryService.addOrRemoveFavoris(idAlbumSpotify, favoris);
        return true;
    }

    @GetMapping(value="/library/addTag/{idAlbum}/{tagAdd}")
    public boolean addTag(@PathVariable("idAlbum") Long idAlbumSpotify, @PathVariable("tagAdd") String tagAdd){
        albumService.addTagToAlbum(idAlbumSpotify, tagAdd);
        return true;
    }
}
