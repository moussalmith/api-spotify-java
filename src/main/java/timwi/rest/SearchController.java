package timwi.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import timwi.domain.Model;
import timwi.service.SearchService;

import java.util.List;

/**
 * 06/05/2021 - 09:25
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
@RestController
@RequestMapping(value = "api")
public class SearchController {
    private final SearchService searchService;

    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }


    @RequestMapping({"/"})
    public String index() {
        return "index";
    }

    @GetMapping(value="/search/{criteria}")
    public List<Model> search(@PathVariable("criteria") String criteria){
        List<Model> result = searchService.searchAlbums(criteria,  null);

        return result;
    }
}
