package timwi.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mongodb.lang.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 06/05/2021 - 06:14
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public class Tag implements Serializable {
    private static final long serialVersionUID = 7901048489335996904L;

    @Id
    private Long id;

    @Field(name="name")
    @NonNull
    private String name;

    @DBRef
    @JsonIgnore
    private Set<Album> albums = new HashSet<Album>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(Set<Album> albums) {
        this.albums = albums;
    }
}
