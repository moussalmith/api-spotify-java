package timwi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 06/05/2021 - 06:11
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public class Album implements Serializable {
    private static final long serialVersionUID = 7901048489335996904L;

    @Id
    private Long id;

    @Field(name="id_album_spotify")
    private String idAlbumSpotify;

    @Field(name="is_favoris")
    private boolean isFavoris;

    @DBRef
    @Field("user")
    @JsonIgnoreProperties(value = "factures", allowSetters = true)
    private Set<Tag> tags = new HashSet<Tag>();

    public Album(String idAlbumSpotify, boolean isFavoris, Set<Tag> tags) {
        this.idAlbumSpotify = idAlbumSpotify;
        this.isFavoris = isFavoris;
        this.tags = tags;
    }

    public Album(String idAlbumSpotify) {
        this.idAlbumSpotify = idAlbumSpotify;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdAlbumSpotify() {
        return idAlbumSpotify;
    }

    public void setIdAlbumSpotify(String idAlbumSpotify) {
        this.idAlbumSpotify = idAlbumSpotify;
    }

    public boolean isFavoris() {
        return isFavoris;
    }

    public void setFavoris(boolean favoris) {
        isFavoris = favoris;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }
}
