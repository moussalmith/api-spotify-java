package timwi.domain;

import com.wrapper.spotify.enums.AlbumType;
import com.wrapper.spotify.enums.ModelObjectType;
import com.wrapper.spotify.model_objects.specification.ArtistSimplified;
import com.wrapper.spotify.model_objects.specification.Image;

import java.util.ArrayList;
import java.util.List;

/**
 * 06/05/2021 - 06:34
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public class Model {
    private AlbumType albumType;
    private ArtistSimplified[] artists;
    private String id;
    private Image[] images;
    private String name;
    private ModelObjectType type;
    private boolean isInAlbumLibrary;
    private boolean isFavoris;
    private List<Tag> tags = new ArrayList<Tag>();

    public AlbumType getAlbumType() {
        return albumType;
    }

    public void setAlbumType(AlbumType albumType) {
        this.albumType = albumType;
    }

    public ArtistSimplified[] getArtists() {
        return artists;
    }

    public void setArtists(ArtistSimplified[] artists) {
        this.artists = artists;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Image[] getImages() {
        return images;
    }

    public void setImages(Image[] images) {
        this.images = images;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ModelObjectType getType() {
        return type;
    }

    public void setType(ModelObjectType type) {
        this.type = type;
    }

    public boolean isInAlbumLibrary() {
        return isInAlbumLibrary;
    }

    public void setInAlbumLibrary(boolean inAlbumLibrary) {
        isInAlbumLibrary = inAlbumLibrary;
    }

    public boolean isFavoris() {
        return isFavoris;
    }

    public void setFavoris(boolean favoris) {
        isFavoris = favoris;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
