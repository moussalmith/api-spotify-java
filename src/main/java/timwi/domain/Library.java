package timwi.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 06/05/2021 - 06:17
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public class Library implements Serializable {
    private static final long serialVersionUID = 7901048489335996904L;

    @Id
    private Long id;

    @DBRef
    private Set<Album> albums = new HashSet<>();

    public Library() {
    }

    public Library(Set<Album> albums) {
        this.albums = albums;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(Set<Album> albums) {
        this.albums = albums;
    }
}
