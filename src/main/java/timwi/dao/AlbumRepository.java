package timwi.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import timwi.domain.Album;

import java.util.Optional;

/**
 * 06/05/2021 - 06:20
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public interface AlbumRepository extends MongoRepository<Album, Long> {
    Optional<Album> findById(Long id);
    //Album findByFavorisNotContains(String id)
    Album findByFavoris(boolean b);
}
