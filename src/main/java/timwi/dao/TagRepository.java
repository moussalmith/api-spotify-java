package timwi.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import timwi.domain.Tag;

import java.util.Optional;

/**
 * 06/05/2021 - 06:22
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public interface TagRepository extends MongoRepository<Tag, Long> {
    Optional<Tag> findByName(String name);
}
