package timwi.dao;

import org.springframework.data.mongodb.repository.MongoRepository;
import timwi.domain.Album;
import timwi.domain.Library;

import java.util.Optional;
import java.util.Set;

/**
 * 06/05/2021 - 06:21
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public interface LibraryRepository extends MongoRepository<Library, Long> {
    Library findByAlbumsExists(Set<Optional<Album>> albums);
}
