package timwi.config;

import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.credentials.ClientCredentials;
import com.wrapper.spotify.requests.authorization.client_credentials.ClientCredentialsRequest;
import org.apache.hc.core5.http.ParseException;
import org.springframework.beans.factory.annotation.Value;

import java.io.IOException;

/**
 * 06/05/2021 - 06:35
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public class SpotifyConfig {

    @Value("${CLIENT_ID}")
    private static final   String CLIENT_ID = "" ;
    @Value("${CLIENT_SECRET}")
    private static final String CLIENT_SECRET = "";

    public static SpotifyApi auth() {

        SpotifyApi spotifyApi = new SpotifyApi.Builder().setClientId(CLIENT_ID).setClientSecret(CLIENT_SECRET).build();
        ClientCredentialsRequest clientCredentialsRequest = spotifyApi.clientCredentials()
                .build();
        try {
            ClientCredentials clientCredentials = clientCredentialsRequest.execute();
            spotifyApi.setAccessToken(clientCredentials.getAccessToken());
            System.out.println("Reçu un access token! " + clientCredentials.getAccessToken());
        } catch (SpotifyWebApiException | IOException | ParseException e) {

        }

        return spotifyApi;
    }
}
