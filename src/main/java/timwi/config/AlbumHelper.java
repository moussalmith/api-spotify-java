package timwi.config;

import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.specification.Album;
import com.wrapper.spotify.requests.data.albums.GetSeveralAlbumsRequest;
import org.apache.hc.core5.http.ParseException;

import java.io.IOException;

/**
 * 06/05/2021 - 06:50
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public class AlbumHelper extends SpotifyConfig{

    public static Album[] getAlbums(String[] ids) {
        SpotifyApi spotifyApi = auth();

        GetSeveralAlbumsRequest getSeveralAlbumsRequest = spotifyApi.getSeveralAlbums(ids)
                .market(CountryCode.SE)
                .build();

        Album[] albums = null;
        try {
            albums = getSeveralAlbumsRequest.execute();

        } catch (SpotifyWebApiException | IOException | ParseException e) {

        }
        return albums;
    }

}
