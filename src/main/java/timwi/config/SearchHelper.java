package timwi.config;

import com.neovisionaries.i18n.CountryCode;
import com.wrapper.spotify.SpotifyApi;
import com.wrapper.spotify.enums.ModelObjectType;
import com.wrapper.spotify.exceptions.SpotifyWebApiException;
import com.wrapper.spotify.model_objects.special.SearchResult;
import com.wrapper.spotify.model_objects.specification.AlbumSimplified;
import com.wrapper.spotify.model_objects.specification.Paging;
import com.wrapper.spotify.requests.data.search.SearchItemRequest;
import org.apache.hc.core5.http.ParseException;

import java.io.IOException;

/**
 * 06/05/2021 - 06:53
 *
 * @author moussalmith@gmail.com
 * @project timwi-api-backend
 */
public class SearchHelper {
    public static Paging<AlbumSimplified> getAlbumSearchItems(String criteria) {
        SpotifyApi spotifyApi = SpotifyConfig.auth();

        SearchItemRequest searchItemRequest = spotifyApi.searchItem(criteria, ModelObjectType.ALBUM.getType())
                .market(CountryCode.SE)
                .limit(20)
                .offset(0)
                .build();
        Paging<AlbumSimplified> albums = null;
        try {
            SearchResult searchResult = searchItemRequest.execute();
            if(searchResult != null) {
                albums = searchResult.getAlbums();
            }
        } catch (SpotifyWebApiException | IOException | ParseException e) {

        }
        return albums;
    }

}
